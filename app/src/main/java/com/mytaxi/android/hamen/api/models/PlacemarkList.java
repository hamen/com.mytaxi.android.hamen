package com.mytaxi.android.hamen.api.models;

import java.util.List;

import lombok.Data;

@Data
public class PlacemarkList {

    public List<Placemark> placemarks;
}
