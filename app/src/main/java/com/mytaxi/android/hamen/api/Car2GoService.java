package com.mytaxi.android.hamen.api;

import com.mytaxi.android.hamen.api.models.Placemark;
import com.mytaxi.android.hamen.api.models.PlacemarkList;

import java.util.List;

import retrofit.http.GET;
import rx.Observable;

public interface Car2GoService {

    @GET("/vehicles.json")
    public Observable<PlacemarkList> getVehicles();
}
