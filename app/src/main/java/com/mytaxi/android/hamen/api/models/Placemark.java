package com.mytaxi.android.hamen.api.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Data;

@Data
public class Placemark implements Parcelable {

    @Expose
    private String address;

    @Expose
    private List<Double> coordinates = new ArrayList<Double>();

    @Expose
    private String exterior;

    @Expose
    private Integer fuel;

    @Expose
    private String interior;

    @Expose
    private String name;

    @Expose
    private String vin;

    @Expose
    private String engineType;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.address);
        dest.writeList(this.coordinates);
        dest.writeString(this.exterior);
        dest.writeValue(this.fuel);
        dest.writeString(this.interior);
        dest.writeString(this.name);
        dest.writeString(this.vin);
        dest.writeString(this.engineType);
    }

    private Placemark(Parcel in) {
        this.address = in.readString();
        this.coordinates = new ArrayList<Double>();
        in.readList(this.coordinates, Double.class.getClassLoader());
        this.exterior = in.readString();
        this.fuel = (Integer) in.readValue(Integer.class.getClassLoader());
        this.interior = in.readString();
        this.name = in.readString();
        this.vin = in.readString();
        this.engineType = in.readString();
    }

    public static final Parcelable.Creator<Placemark> CREATOR = new Parcelable.Creator<Placemark>() {
        public Placemark createFromParcel(Parcel source) {
            return new Placemark(source);
        }

        public Placemark[] newArray(int size) {
            return new Placemark[size];
        }
    };
}