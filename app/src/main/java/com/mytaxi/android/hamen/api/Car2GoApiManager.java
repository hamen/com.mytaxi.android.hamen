package com.mytaxi.android.hamen.api;

import com.mytaxi.android.hamen.api.models.Placemark;
import com.mytaxi.android.hamen.api.models.PlacemarkList;

import java.util.List;

import retrofit.RestAdapter;
import rx.Observable;
import rx.functions.Func1;

public class Car2GoApiManager {

    private final Car2GoService mService;

    public Car2GoApiManager() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://redirect.mytaxi.net/car2go")
                .build();
        mService = restAdapter.create(Car2GoService.class);
    }

    public Observable<List<Placemark>> getVehicles() {

        return mService.getVehicles()
                .map(new Func1<PlacemarkList, List<Placemark>>() {
                    @Override
                    public List<Placemark> call(PlacemarkList placemarkList) {
                        return placemarkList.getPlacemarks();
                    }
                });
    }
}
