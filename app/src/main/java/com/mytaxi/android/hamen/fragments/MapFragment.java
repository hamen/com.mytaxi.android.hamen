package com.mytaxi.android.hamen.fragments;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import com.mytaxi.android.hamen.Car2GoApp;
import com.mytaxi.android.hamen.R;
import com.mytaxi.android.hamen.adapters.PopupAdapter;
import com.mytaxi.android.hamen.api.models.Placemark;

import android.app.Fragment;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MapFragment extends Fragment {

    private static View view;

    private static GoogleMap mMap;

    private List<Placemark> mMarkers = new ArrayList<Placemark>();

    private int mMarker2zoom;

    public static Fragment newInstance(List<Placemark> markers, int markerToZoom) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("MARKERS", (java.util.ArrayList<? extends android.os.Parcelable>) markers);
        bundle.putInt("MARKER2ZOOM", markerToZoom);
        MapFragment fragment = new MapFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    public static Fragment newInstance(List<Placemark> markers) {
        return newInstance(markers, 0);
    }

    /**
     * ** Sets up the map if it is possible to do so ****
     */
    public static void setUpMapIfNeeded(List<Placemark> markers, int marker2zoom) {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) Car2GoApp.getActivity().getSupportFragmentManager().findFragmentById(R.id.location_map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap(markers, marker2zoom);
            }
        }
    }

    private static void setUpMap(List<Placemark> markers, int marker2zoom) {
        // For showing a move to my location button
        mMap.setMyLocationEnabled(true);

        for (Placemark placemark : markers) {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(placemark.getCoordinates().get(1), placemark.getCoordinates().get(0)))
                    .title(placemark.getName())
                    .snippet(new Gson().toJson(placemark)));
        }

        // For zooming automatically to the last Dropped PIN Location
        mMap.animateCamera(CameraUpdateFactory
                .newLatLngZoom(
                        new LatLng(markers.get(marker2zoom).getCoordinates().get(1), markers.get(marker2zoom).getCoordinates().get(0)),
                        16.0f));

        mMap.setInfoWindowAdapter(new PopupAdapter(Car2GoApp.getActivity().getLayoutInflater()));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.fragment_maps, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        Bundle bundle = getArguments();
        mMarkers = bundle.getParcelableArrayList("MARKERS");
        mMarker2zoom = bundle.getInt("MARKER2ZOOM");

        setUpMapIfNeeded(mMarkers, mMarker2zoom);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (mMap != null) {
            setUpMap(mMarkers, mMarker2zoom);
        }

        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) Car2GoApp.getActivity().getSupportFragmentManager().findFragmentById(R.id.location_map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap(mMarkers, mMarker2zoom);
            }
        }
    }

    /**
     * * The mapfragment's id must be removed from the FragmentManager *** or else if the same it is passed on the next time then *** app will crash
     * ***
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            if (mMap != null) {
                Car2GoApp.getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .remove(Car2GoApp.getActivity().getSupportFragmentManager().findFragmentById(R.id.location_map))
                        .commit();
                mMap = null;
            }
        } catch (IllegalStateException e) {
            // Just gimme a break!
        }
    }
}