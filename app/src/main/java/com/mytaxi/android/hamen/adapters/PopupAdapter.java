package com.mytaxi.android.hamen.adapters;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;

import com.mytaxi.android.hamen.R;
import com.mytaxi.android.hamen.api.models.Placemark;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class PopupAdapter implements GoogleMap.InfoWindowAdapter {

    private View popup = null;

    private LayoutInflater inflater = null;

    public PopupAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return (null);
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoContents(Marker marker) {
        if (popup == null) {
            popup = inflater.inflate(R.layout.fragment_vehicles_gridview_item, null);
        }
        ViewHolder holder = new ViewHolder(popup);

        Placemark vehicle = new Gson().fromJson(marker.getSnippet(), Placemark.class);

        holder.name.setText(vehicle.getName());
        holder.address.setText(vehicle.getAddress());

        holder.interior.setImageResource(vehicle.getInterior().equals("GOOD") ? R.drawable.ic_action_emo_laugh : R.drawable.ic_action_emo_cry);
        holder.exterior.setImageResource(vehicle.getExterior().equals("GOOD") ? R.drawable.ic_action_emo_laugh : R.drawable.ic_action_emo_cry);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, 100);
        lp.weight = vehicle.getFuel();

        int fuelBackground;
        int fuel = vehicle.getFuel();
        if (fuel < 30) {
            fuelBackground = R.color.red;
        } else if (fuel > 30 && fuel < 60) {
            fuelBackground = R.color.yellow;
        } else {
            fuelBackground = R.color.green;
        }
        holder.fuel.setBackgroundResource(fuelBackground);
        holder.fuel.setLayoutParams(lp);

        return (popup);
    }

    static class ViewHolder {

        @InjectView(R.id.name)
        TextView name;

        @InjectView(R.id.address)
        TextView address;

        @InjectView(R.id.interior)
        ImageView interior;

        @InjectView(R.id.exterior)
        ImageView exterior;

        @InjectView(R.id.fuel)
        LinearLayout fuel;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}