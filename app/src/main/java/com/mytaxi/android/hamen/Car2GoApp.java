package com.mytaxi.android.hamen;

import android.app.Application;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(prefix = "m")
public class Car2GoApp extends Application {

    @Getter
    @Setter
    private static MainActivity mActivity;
}
