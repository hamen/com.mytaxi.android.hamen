package com.mytaxi.android.hamen.adapters;

import com.mytaxi.android.hamen.R;
import com.mytaxi.android.hamen.api.models.Placemark;

import android.content.pm.LabeledIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.xml.validation.Validator;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class VehiclesListAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;

    private List<Placemark> mVehicles = new ArrayList<Placemark>();

    public VehiclesListAdapter(LayoutInflater inflater, List<Placemark> vehicles) {
        mInflater = inflater;
        mVehicles = vehicles;
    }

    @Override
    public int getCount() {
        return mVehicles.size();
    }

    @Override
    public Placemark getItem(int position) {
        return mVehicles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.fragment_vehicles_gridview_item, parent, false);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Placemark vehicle = getItem(position);

        holder.name.setText(vehicle.getName());
        holder.address.setText(vehicle.getAddress());

        holder.interior.setImageResource(vehicle.getInterior().equals("GOOD") ? R.drawable.ic_action_emo_laugh : R.drawable.ic_action_emo_cry);
        holder.exterior.setImageResource(vehicle.getExterior().equals("GOOD") ? R.drawable.ic_action_emo_laugh : R.drawable.ic_action_emo_cry);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, 100);
        lp.weight = vehicle.getFuel();

        int fuelBackground;
        int fuel = vehicle.getFuel();
        if (fuel < 30) {
            fuelBackground = R.color.red;
        } else if (fuel > 30 && fuel < 60) {
            fuelBackground = R.color.yellow;
        } else {
            fuelBackground = R.color.green;
        }
        holder.fuel.setBackgroundResource(fuelBackground);
        holder.fuel.setLayoutParams(lp);

        return convertView;
    }

    public void setVehicles(List<Placemark> placemarks) {
        mVehicles.clear();
        mVehicles.addAll(placemarks);
        notifyDataSetChanged();
    }

    static class ViewHolder {

        @InjectView(R.id.name)
        TextView name;

        @InjectView(R.id.address)
        TextView address;

        @InjectView(R.id.interior)
        ImageView interior;

        @InjectView(R.id.exterior)
        ImageView exterior;

        @InjectView(R.id.fuel)
        LinearLayout fuel;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
