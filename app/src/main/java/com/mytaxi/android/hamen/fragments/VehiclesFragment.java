package com.mytaxi.android.hamen.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.mytaxi.android.hamen.Car2GoApp;
import com.mytaxi.android.hamen.R;
import com.mytaxi.android.hamen.adapters.VehiclesListAdapter;
import com.mytaxi.android.hamen.api.Car2GoApiManager;
import com.mytaxi.android.hamen.api.models.Placemark;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class VehiclesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @InjectView(R.id.gridView)
    GridView mGridView;

    @InjectView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private VehiclesListAdapter mAdapter;

    private List<Placemark> mVehicles = new ArrayList<Placemark>();

    public static VehiclesFragment newInstance() {
        VehiclesFragment fragment = new VehiclesFragment();
        return fragment;
    }

    public VehiclesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehicles, container, false);
        ButterKnife.inject(this, view);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorScheme(R.color.car2go, R.color.white, R.color.car2go, R.color.white);

        mAdapter = new VehiclesListAdapter(inflater, mVehicles);
        mGridView.setAdapter(mAdapter);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Car2GoApp.getActivity().getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, MapFragment.newInstance(mVehicles, position))
                        .commit();
            }
        });
        refreshList();

        return view;
    }

    private void refreshList() {

        mSwipeRefreshLayout.setRefreshing(true);

        Car2GoApiManager car2GoApiManager = new Car2GoApiManager();
        car2GoApiManager.getVehicles()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Placemark>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("MYTAXI", e.toString());
                    }

                    @Override
                    public void onNext(List<Placemark> placemarks) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        mVehicles = placemarks;
                        Car2GoApp.getActivity().setVehicles(placemarks);
                        mAdapter.setVehicles(placemarks);
                    }
                });

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

}
